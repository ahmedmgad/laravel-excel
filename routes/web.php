<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function (){
   return View('front.index');
});
Route::post('/','Front\FileController@uploadFile');


Route::get('read','Front\FileController@readFile');
Route::post('read','Front\FileController@processFile');
