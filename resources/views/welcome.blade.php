@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        {{Form::open()}}
                            @if(session()->has('url'))
                            <div class="alert alert-success">
                                <div class="row">
                                    <div class="col-md-6">
                                        Shorten Url : <a href="{{url(session()->get('url')->slug)}}">{{url(session()->get('url')->slug)}}</a>
                                    </div>
                                    <div class="col-md-6">
                                        States Url : <a href="{{url(session()->get('url')->slug.'+')}}">{{url(session()->get('url')->slug.'+')}}</a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-xs-12">
                                {{Form::text('url',null,['class'=>'form-control','placeholder'=>'URL'])}}
                                <div class="text-right">
                                    <a href="javascript:;" class="show-advanced-options">advanced options</a>
                                </div>
                            </div>
                            <div class="col-xs-12" id="advanced-area" style="display: none;">
                                <div class="row">

                                    <div class="col-md-4 col-sm-6">
                                        <h6>Custom Slug</h6>
                                        <p>If you need a custom alias, you can enter it below.</p>
                                        <div class="input-group">
                                            {{Form::text('slug',null,['class'=>'form-control','placeholder'=>'Custom Slug here','autocomplete'=>'off'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <h6>Link Expiration</h6>
                                        <p>Expiration date to disable the URL after this date.</p>
                                        <div class="input-group">
                                            {{Form::text('expiry_date',null,['class'=>'form-control','placeholder'=>'DD/MM/YYYY','autocomplete'=>'off'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <h6>Password Protect</h6>
                                        <p>By adding a password, you can restrict the access.</p>
                                        <div class="input-group">
                                            {{Form::password('password',['class'=>'form-control'])}}
                                            <span class="input-group-btn"> <button class="btn btn-default" onclick="return showPassword()" type="button">show</button> </span>
                                        </div>
                                    </div>

                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6>Redirections</h6>
                                    </div>
                                </div>
                                <div class="row" id="redirections" style="margin-bottom:10px ;">

                                    <div class="col-md-3 col-sm-6">
                                        <h6>Country</h6>
                                        <div class="input-group">
                                            {{Form::select('geo[country][]',Countries::orderBy('name')->pluck('name','iso_3166_2'),null,['class'=>'form-control','placeholder'=>'All Countries'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6">
                                        <h6>Device</h6>
                                        <div class="input-group">
                                            {{Form::select('geo[device][]',['iPhone','Android','BlackBerry','Windows Phone'],null,['class'=>'form-control','placeholder'=>'All Devices'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6">
                                        <h6>Url</h6>
                                        <div class="input-group">
                                            {{Form::text('geo[url][]',null,['class'=>'form-control'])}}
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6">
                                        <h6> add more </h6>
                                        <button type="button" class="btn btn-success addMore">+</button>
                                    </div>
                                </div>
                                <div id="redirections-area"></div>
                                <hr/>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-lg btn-success" type="submit">Short</button>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styles')
    {{Html::style('plugins/datepicker/dist/datepicker.min.css')}}
@endsection
@section('scripts')
    {{Html::script('plugins/datepicker/dist/datepicker.min.js')}}
    <script>
        $('a.show-advanced-options').on('click',function () {
            if ( $( "#advanced-area:first" ).is( ":hidden" ) ) {
                $( "#advanced-area" ).slideDown( "slow" );
            } else {
                $( "#advanced-area" ).slideUp('slow');
            }
        });
        $('input[name="expiry_date"]').datepicker({
            format:"DD/MM/YYYY",
            startDate: new Date() // Or '02/14/2014'
        });

        function showPassword(){
            password = $('input[name="password"]');
            if(password.attr('type') === 'password'){
                password.attr('type','text');
            }else{
                password.attr('type','password');
            }
        }

        $('.addMore').on('click',function () {
            redirections = $('#redirections').clone();
            redirections.appendTo("#redirections-area");
            redirections.find('h6').remove();
            redirectionsBtn = redirections.find('.addMore').text('x');
            redirectionsBtn.removeClass('btn-success').removeClass('addMore').addClass('btn-danger').addClass('remove');
            redirections.find('select')[0].selectedIndex = 0;
            redirections.find('input').val('');
        });

        $(document).on('click','.remove',function () {
            $(this).parent().parent().remove();
        })
    </script>
@stop

