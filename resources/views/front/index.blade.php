@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <li>{{$errors->first()}}</li>
                        </div>
                    @endif


                    <div class="card-body">
                        {{Form::open(['files'=>true])}}

                        <div class="col-xs-12">
                            {{Form::file('file',['class'=>'form-control','placeholder'=>'URL'])}}
                        </div>
                        <br>

                        <div class="col-xs-12">
                            <button class="btn btn-lg btn-success" type="submit">Upload</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styles')
    {{Html::style('plugins/datepicker/dist/datepicker.min.css')}}
@endsection
@section('scripts')
    {{Html::script('plugins/datepicker/dist/datepicker.min.js')}}
    <script>
        $('a.show-advanced-options').on('click',function () {
            if ( $( "#advanced-area:first" ).is( ":hidden" ) ) {
                $( "#advanced-area" ).slideDown( "slow" );
            } else {
                $( "#advanced-area" ).slideUp('slow');
            }
        });
        $('input[name="expiry_date"]').datepicker({
            format:"DD/MM/YYYY",
            startDate: new Date() // Or '02/14/2014'
        });

        function showPassword(){
            password = $('input[name="password"]');
            if(password.attr('type') === 'password'){
                password.attr('type','text');
            }else{
                password.attr('type','password');
            }
        }

        $('.addMore').on('click',function () {
            redirections = $('#redirections').clone();
            redirections.appendTo("#redirections-area");
            redirections.find('h6').remove();
            redirectionsBtn = redirections.find('.addMore').text('x');
            redirectionsBtn.removeClass('btn-success').removeClass('addMore').addClass('btn-danger').addClass('remove');
            redirections.find('select')[0].selectedIndex = 0;
            redirections.find('input').val('');
        });

        $(document).on('click','.remove',function () {
            $(this).parent().parent().remove();
        })
    </script>
@stop

