@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                @if($errors->any())
                <div class="alert alert-danger">
                    <li>{{$errors->first()}}</li>
                </div>
                @endif

                {{Form::open()}}
                <div class="card-body">
                    <div class="row">
                        @foreach($myKeys as $key)
                        <div class="col-md-2 col-sm-6">
                            <label>{{$key}}</label>
                            <div class="input-group">
                                {{Form::select('record['.$key.']',$excelKeys,null,['required','class'=>'form-control','placeholder'=>'please select ...'])}}
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <br>
                    <div class="col-xs-12">
                        {{Form::hidden('file',request()->get('file'))}}
                        <button class="btn btn-lg btn-success" type="submit">Process</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection

