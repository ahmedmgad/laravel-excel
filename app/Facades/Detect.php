<?php
namespace App\Facades;

use App\Services\DetectService;
use Illuminate\Support\Facades\Facade;

/**
 * DetectFacade
 *
 */
class Detect extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return DetectService::class; }
}

