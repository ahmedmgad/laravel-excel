<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function uploadFile(Request $request)
    {
        if($request->hasFile('file')){
            if(!in_array(strtolower($request->file('file')->getClientOriginalExtension()),['xls','xlsx'])){
                return back()->withErrors(['file not excel']);
            }
            $filename = time().'.'.$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move('uploads/tmp/',$filename);
            return redirect('read?file='.$filename);
        }
        return back()->withErrors(['not uploaded']);
    }

    public function readFile(Request $request)
    {
        if(!$request->has('file')){
            return redirect('/');
        }
        if(!file_exists('uploads/tmp/'.$request->get('file'))){
            return redirect('/');
        }
        //This would be database columns or what ever you want
        $myKeys     = ['name','email','mobile','date','time','state'];
        $reader     = \Excel::load('uploads/tmp/'.$request->get('file'));
        $excelKeys  = $reader->first()->keys()->toArray();
        return View('front.readfile',compact('myKeys','excelKeys'));
    }

    public function processFile(Request $request)
    {
        $map  = array_flip($request->get('record'));
        //dd($map);
        $file = 'uploads/tmp/'.$request->get('file');
        $reader = \Excel::load($file,function ($reader) use($map){
            foreach($reader->toArray() as $i=>$item){
                $item = array_values($item);
                foreach ($map as $key=>$value){
                    $transformed[$i][$map[$key]] = $item[$key];
                }
            }
            //you can use $transformed array to insert it into db or do whatever
            //dd($transformed);
        });
        //dd($this->res);

    }
}
